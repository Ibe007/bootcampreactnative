// di index.js
// jawaban pertama
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

var timeAvailable = 10000;

readBooks(timeAvailable, books[0], function(time) {
    readBooks(time, books[1], function(time) {
        readBooks(time, books[2], function(time){
            readBooks(time, books[0], function(time){
                return time;
            })
        })
    })
});

// jawaban 0
let t = 10000
let i = 0;

function panggil() {
    readBooks(t, books[i], function(sisaWaktu){
        t = sisaWaktu
        i++
        if(i< books.length)
        panggil()

    })
}

panggil();
