//  Soal If-Else

//Input
var nama    = ''
var peran   = ''

// Variabel
P       = 'kamu dapat melihat siapa yang menjadi werewolf!'
G       = 'kamu akan membantu melindungi temanmu dari serangan werewolf.'
W       = 'Kamu akan memakan mangsa setiap malam!'


// Output untuk Input nama = '' dan peran = ''
if (nama == '' && peran == '') {
    console.log ("Nama harus diisi!")
}
 
//Output untuk Input nama = 'John' dan peran = ''
    else if (nama != '' && peran == ''){
        console.log("Halo " + nama +  ", Pilih peranmu untuk memulai game!")
    }
 
//Output untuk Input nama = 'Jane' dan peran 'Penyihir'
    else if (nama != '' && peran == 'Penyihir'){
        console.log("Selamat datang di Dunia Werewolf, " + nama)
        console.log('Halo' + ' ' + peran + ' ' + nama + ',' + ' ' + P)
    }
// //Output untuk Input nama = 'Jenita' dan peran 'Guard'
    else if (nama != '' && peran == 'Guard'){
        console.log("Selamat datang di Dunia Werewolf, " + nama)
        console.log('Halo' + ' ' + peran + ' ' + nama + ',' + ' ' + G)
    }
// //Output untuk Input nama = 'Junaedi' dan peran 'Werewolf'
else {
    console.log("Selamat datang di Dunia Werewolf, " + nama)
    console.log('Halo' + ' ' + peran + ' ' + nama + ',' + ' ' + W)
}

// Soal Switch Case
var hari    = 1;
var bulan   = 1;
var tahun   = 1998;

if (hari >= 1 && hari <= 31 && bulan >= 1 && bulan <= 12 && tahun >= 1900 && tahun <= 2200)
{
    
    switch (bulan) 
    {
        case 1: {console.log (hari+' Januari '+tahun); break;}
        case 2: {console.log (hari+' Februari '+tahun); break;}
        case 3: {console.log (hari+' Maret '+tahun); break;}
        case 4: {console.log (hari+' April '+tahun); break;}
        case 5: {console.log (hari+' Mei '+tahun); break;}
        case 6: {console.log (hari+' Juni '+tahun); break;}
        case 7: {console.log (hari+' Juli '+tahun); break;}
        case 8: {console.log (hari+' Agustus '+tahun); break;}
        case 9: {console.log (hari+' September '+tahun); break;}
        case 10: {console.log (hari+' Oktober '+tahun); break;}
        case 11: {console.log (hari+' November '+tahun); break;}
        case 12: {console.log (hari+' Desember '+tahun); break;}
    }
}
