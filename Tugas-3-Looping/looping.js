//Soal No 1. While Looping
var loop1 = true;
var angka1 = 0;
var batasAngka1 = 20;
var angka2 = 22;
var batasAngka2 = 2;
var loncat = 2;

while (batasAngka1 > 0) {
    if (angka1 == 0) {
        console.log ('LOOPING PERTAMA');
    }
    batasAngka1  -= loncat;
    angka1 += loncat;
    console.log (angka1 , ' - ' , 'I Love Coding');
}
while (batasAngka2 < 22) {
    if (angka2 == 22){
        console.log ('LOOPING KEDUA');
    }
    batasAngka2 += loncat;
    angka2 -= loncat;
    console.log (angka2 , ' - ' , ' I will become a mobile developer');
}
// Latihan For Looping
console.log ('OUTPUT')
for (var i = 1; i <= 20; i++) {
    if (i % 3 !== 0 && i % 2 !== 0) {
        console.log (i , ' - Santai');
    } else if ( i % 2 === 0) {
        console.log (i , " - Berkualitas");
    } else if (i % 3 === 0 && i % 2 === 1) {
        console.log (i , ' - I Love Coding');
    }
}
// persegi panjang
console.log ('PERSEGI PANJANG')
var x = ''
for (var J = 0; J < 4; J++) {
    x = '';
    for ( var k = 0; k < 8; k++) {
        x += '#';
    }
    console.log (x)
    
}
// Tangga
console.log ('TANGGA')
var x = ''
for (var l = 1; l <= 7; l++) {
    for ( var m = 0; m < l; m++) {
        x += '#';
    }
    console.log (x)
    x = '';
}
// Catur
console.log ('CATUR')
var first = ' ';
var second = '#';
var x = '';
for (var J = 0; J <= 8; J++) {
    x = '';
    for ( var k = 0; k <= 7; k++) {
        if ( k % 2 !== 0 && J % 2 !== 0){
            x += first;
        } else if (k % 2 !== 0 && J % 2 === 0){
            x += second;
        } else if (k % 2 === 0 && J % 2 !== 0){
            x += second;
        } else if (k % 2 === 0 && J % 2 === 0){
            x += first;
        }
    }

    console.log (x)
    
}