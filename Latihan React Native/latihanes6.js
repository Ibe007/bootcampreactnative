let x = 1;
 
if (x === 1) {
  let x = 2;
 
  console.log(x);
  // expected output: 2
}
 
console.log(x); // 1 

// const number = 42;
// number = 100; // Uncaught TypeError: Assignment to constant variable.

// Funtion

const appFunction = ( triak ) => { 
   return triak
} 
console.log(appFunction("Nama Saya Ibrohim"))

let studentName = {
  firstName: 'Peter',
  lastName: 'Parker'
};

// Destruturing

const { firstName, lastName } = studentName;

console.log(firstName); // Peter
console.log(lastName); // Parker 

// Rest Parameters
 
let scores = ['98', '95', '93', '90', '87', '85']
let [first, second, third, ...restOfScores] = scores;
 
console.log(first) // 98
console.log(second) // 95
console.log(third) // 93
console.log(restOfScores) // [90, 87, 85] 

//spread operator

let array1 = ['one', 'two']
let array2 = ['three', 'four']
let array3 = ['five', 'six']

// ES6 Way 
 
let combinedArray = [...array1, ...array2, ...array3]
console.log(combinedArray) // ['one', 'two', 'three', 'four', 'five', 'six'] 

// Template Literals

const firstName = "Yena"
const lastName = "Violet"
const teamName = "The Dancer"

const theString = `${firstName} ${lastName} ${teamName}`

console.log(theString)