//Latihan While Looping
var loop1 = true;
var angka1 = 0;
var batasAngka1 = 20;
var angka2 = 22;
var batasAngka2 = 2;
var loncat = 2;

while (batasAngka1 > 0) {
    if (angka1 == 0) {
        console.log ('Looping Pertama');
    }
    batasAngka1  -= loncat;
    angka1 += loncat;
    console.log (angka1 , ' - ' , 'I Love Coding');
}
while (batasAngka2 < 22) {
    if (angka2 == 22){
        console.log ('Looping Kedua');
    }
    batasAngka2 += loncat;
    angka2 -= loncat;
    console.log (angka2 , ' - ' , ' Aku Suka Koding');
}
// Latihan For Looping
console.log ('For Loop')
for (var i = 1; i <= 20; i++) {
    if (i % 3 !== 0 && i % 2 !== 0) {
        console.log (i , ' - Santai');
    } else if ( i % 2 === 0) {
        console.log (i , " - Berkualitas");
    } else if (i % 3 === 0 && i % 2 === 1) {
        console.log (i , ' - I Love Coding');
    }
}
// persegi panjang
console.log ('Persegi Panjang')
var x = ''
for (var J = 0; J < 4; J++) {
    x = '';
    for ( var k = 0; k < 8; k++) {
        x += '#';
    }
    console.log (x)
    
}
// Tangga
console.log ('Tangga')
var x = ''
for (var l = 1; l <= 7; l++) {
    for ( var m = 0; m < l; m++) {
        x += '#';
    }
    console.log (x)
    x = '';
}
// Catur
